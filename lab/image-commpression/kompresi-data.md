[[_TOC_]]

# Image Compression / Kompresi Dataa
## Apa itu kompresi data ?
- Kompresi berkaitan dengan mengecilkan sebuah ukuran atau memampatkan 
- Proses kompresi data menggunakan pengkodean informasi menggunakan bit atay information-bearing unit yang tentunya perlu lebih rengan dari data representasi yang tidak terkodekan dengan suatu sistem enkoding tertentu
- contoh kompresi yang paling sederhana yang biasa dilakukakn adalah mempersingkat kata contoh : PGRI (Persatuan Guru Republik Indonesia) kata PGRI adalah kompersi dari Persatuan Guru Republik Indonesia
- Kompresi data memiliki keunggulan yang cukup penting dalam kebutuhan memperkecil penyimpanan data, mepercepat pengiriman data, memperkecil kebutuhan bandwith
-Beberapa teknik kompresi ini dapat dilakukan untuk banyak jenis seperti teks/biner, Gambar ( JPEG, PNG, TIFF) Audio (MP3, AAC, RMA, WMA,) dan video (MPEG, H261, H263)

## Jenis kompresi data bersasarkan mode penerima data oleh manusia
- Dialoque Mode : Proses penerimaan data dimana pengirim data seakan berdialog secara realtime, contoh: video conference, hal ini disebabkan karena kompresi harus berada dalam batas penglihatan dan pendengara manusia. terdapat waktu tunda (delay) > 150 ms dimana 50 ms untuk prsoses kompresi dan dekompresi, 100 ms mentranmisi melalaui data dalam jaringan
- Retrieval Mode : Proses penerimaan data tidak dilakukan secara real time 
    - Dapat dilakukan fast forward, dan fast rewind di client 
    - Dapat dilakukan random akses dan dapat bersifat interaktif.

## Jenis kompresi data berdasarkan output
 - Loosy Compression 
    - Teknik kompresi dimana data hasil dekompresi tidak sama dengan data sebelum kompresi namun sudah “cukup” untuk digunakan.Contoh: Mp3, streaming media, JPEG, MPEG, dan WMA.
    - Kelebihan: ukuran file lebih kecil dibanding loseless namun masih tetap memenuhi syarat untuk digunakan.
    - Biasanya teknik ini membuang bagian-bagian data yang sebenarnya tidak begitu berguna, tidak begitu dirasakan, tidak begitu dilihat oleh manusia sehingga manusia masih beranggapan bahwa data tersebut masih bisa digunakan walaupun sudah dikompresi.
    - Misal terdapat image asli berukuran 12,249 bytes, kemudian dilakukan kompresi dengan JPEG kualitas 30 dan berukuran 1,869 bytes berarti image tersebut 85% lebih kecil dan ratio kompresi 15%.
    
- Loseless Compression
    - Teknik kompresi dimana data hasil kompresi dapat didekompres lagi dan hasilnya tepat sama seperti data sebelum proses kompresi. Contoh aplikasi: ZIP, RAR, GZIP, 7-Zip
    - Teknik ini digunakan jika dibutuhkan data setelah dikompresi harus dapat diekstrak/dekompres lagi tepat sama. Contoh pada data teks, data program/biner, beberapa image seperti GIF dan PNG.
    - Kadangkala ada data-data yang setelah dikompresi dengan teknik ini ukurannya menjadi lebih besar
atau sama.

## Kriteria Algoritma dan aplikasi kompresi data 
- Kualitas data hasil enkoding: ukuran lebih kecil, data tidak rusak untuk kompresi lossy.
- Kecepatan, ratio, dan efisiensi proses kompresi dan dekompresi
- Ketepatan proses dekompresi data: data hasil dekompresi tetap sama dengan data sebelum dikompres (kompresi loseless)

## Klasifikasi teknik Kompesi 

### 1. Entropy Encoding 
    - Bersifat loseless
    - Tekniknya tidak berdasarkan media dengan spesifikasi dan karakteristik tertentu namun berdasarkan urutan data.
    - Statistical encoding, tidak memperhatikan semantik data.
    - Mis: Run-length coding, Huffman coding,
    - Arithmetic coding
### 2. Source Coding
    - Bersifat lossy
    - Berkaitan dengan data semantik (arti data) dan media.
    - Mis: Prediction (DPCM, DM), Transformation (FFT, DCT), Layered Coding (Bit position, subsampling, sub-band coding), Vector quantization
### 3. Hybrid Coding 
    - Gabungan antara lossy + loseless mis: JPEG, MPEG, H.261, DVI

## Kompresi citra statis (still image)

Kompresi Citra adalah aplikasi kompresi data yang dilakukan terhadap citra digital dengan tujuan untuk mengurangi redundansi dari data-data yang terdapat dalam citra sehingga dapat disimpan atau ditransmisikan secara efisien.

## Teknik kompresi citra 

### Lossy Compression 
    - Ukuran citra lebih kecil dengan menghilangkan informasi dalam citra asli 
    - Color reduction
        warna-warna mayoritas disimpan dalam color pallete
    - Chroma subsampling
        teknik yang memanfaatkan fakta bahwa mata manusia merasa brightness (luminance) lebih berpengaruh daripada warna (chrominance) itu sendiri, maka dilakukan pengurangan resolusi warna dengan disampling ulang. Biasanya digunakan pada sinyal YUV. Chorma Subsampling terdiri dari 3 komponen: Y (luminance) : U (CBlue) : V (CRed)
    - Transform coding : menggunakan proses yang dikenal Fourier Transform seperti DCT. 
        - Fractal Compression: suatu metode lossy yang biasa digunakan untuk kompresi citra dengan menggunakan kurva fractal, cocok untuk citra pephonann, pegunungan, dan awan 

### Loseless Compression
    - Teknik kompresi citra yang mana informasi citra tidak dihilangkan 
    - teknik ini digunakan untuk proses citra medis 
    - Metode loseless : RLE (Run length Encoding), Entropy Encoding (Huffman, aritmatik) dan Adaptive Dictionary Based (LZW)

## Hal penting dalam kompresi citra 
1. Scability : kualitas proses prengkrompesi pada loseless codec
    - Tipe scability : 
        - Quality progressive : saat gambar di kompres secara perlahan mengalami penurun kualitas
        - Resolution progressive : image di kompresi dengan mengkode resolusi image yang lebih rendah dahulu kedumian menuju resolusi tinggi. 
        - Component progressive : image di kompresi beradasrkan komponennya, mengkose komponen gray baru komponen warnanya. 
2. Region of Interest (ROI) : daerah tertentu diekode dengan kualitias yang lebih tinggi dari daripada yang lain 
3. Meta information : image yang di kompres untuk mendapatkan meta informasi seperti statistik warna, tekstur, small preview,dan beberap informasi tentang copyright. 


# Algoritma image compression 

## 1. Huffman Encoding 

Huffman Encoding adalah Algoritma Kompresi Lossless yang digunakan untuk mengompres data. Ini adalah algoritma yang dikembangkan oleh David A. Huffman saat dia masih menjadi Sc.D. mahasiswa di MIT, dan diterbitkan dalam makalah tahun 1952 “A Method for the Construction of Minimum-Redundancy Codes”.

### Algoritma

Huffman Encoding adalah algoritma yang menggunakan fitur frekuensi (atau probabilitas) dari simbol dan struktur pohon biner. Ini terdiri dari 3 langkah berikut:

- Perhitungan Probabilitas & Pengurutan Simbol
- Transformasi Pohon Biner
- Menetapkan Kode ke Simbol

### Perhitungan Probabilitas & Pengurutan Simbol
![Perhitungan probabilitas dan pengurutan symbol](image-commpression/gambar_1.webp)


Kemudian kita dengan mudah mengurutkan simbol sesuai dengan probabilitasnya yang mewakili setiap simbol sebagai simpul dan menyebutnya "koleksi" kita. Sekarang, kita siap untuk melewati langkah selanjutnya.

![Hasil](image-commpression/gambar_2.webp)
![Hasil](image-commpression/gambar_3.webp)

### Binary Tree Transfomation 

![final huffman tree](image-commpression/gambar_6.webp)


### Assigning Codes to symbols

![assigning code to symbols](image-commpression/gambar_5.webp)


Kita dapat melihat sekilas dan melihat bahkan hanya untuk 21 karakter, perbedaan antara data terkompresi dan non-terkompresi tidak sepele.

### Implementation with python 
Untuk mengimplementasikan Huffman Encoding, kita mulai dengan kelas Node, yang mengacu pada node dari Binary Huffman Tree. Pada intinya, setiap node memiliki simbol dan variabel probabilitas terkait, anak kiri dan kanan dan variabel kode. Variabel kode akan menjadi 0 atau 1 saat kita melakukan perjalanan melalui Pohon Huffman sesuai dengan sisi yang kita pilih (kiri 0, kanan 1)

```python
# A Huffman Tree Node
class Node:
    def __init__(self, prob, symbol, left=None, right=None):
        # Probabilitas
        self.prob = prob

        # simbol
        self.symbol = symbol

        # node kiri
        self.left = left

        # node kanan
        self.right = right

        # tree direction (0/1)
        self.code = ''


```

kita memiliki 3 fungsi pembantu, yang pertama untuk menghitung probabilitas simbol dalam data yang diberikan, yang kedua untuk mendapatkan pengkodean simbol yang akan digunakan setelah Huffman Tree dan yang terakhir untuk mendapatkan output (data yang dikodekan).

```python

""" Fungsi pembantu untuk mencetak kode simbol dengan melakukan proses Huffman Tree"""
codes = dict()

def Calculate_Codes(node, val=''):
    # huffman untuk kode terkini
    newVal = val + str(node.code)

    if(node.left):
        Calculate_Codes(node.left, newVal)
    if(node.right):
        Calculate_Codes(node.right, newVal)

    if(not node.left and not node.right):
        codes[node.symbol] = newVal
         
    return codes        

""" Fungsi pembantu untuk mencetak kode simbol dengan melakukan proses Huffman Tree"""
def Calculate_Probability(data):
    symbols = dict()
    for element in data:
        if symbols.get(element) == None:
            symbols[element] = 1
        else: 
            symbols[element] += 1     
    return symbols

""" Fungsi pembantu untuk mendapatkan encoded"""
def Output_Encoded(data, coding):
    encoding_output = []
    for c in data:
      #  print(coding[c], end = '')
        encoding_output.append(coding[c])
        
    string = ''.join([str(item) for item in encoding_output])    
    return string
        
""" Fungsi pembantu untuk menghitung perbedaan ruang antara data terkompresi dan tidak terkompresi"""    
def Total_Gain(data, coding):
    before_compression = len(data) * 8 # total bit yang disimpan pada data sebelum di kompresi
    after_compression = 0
    symbols = coding.keys()
    for symbol in symbols:
        count = data.count(symbol)
        after_compression += count * len(coding[symbol]) #menghitung sebarapa banyak bit yang di butuhkan oleh simbol secara total 
    print("Space usage before compression (in bits):", before_compression)    
    print("Space usage after compression (in bits):",  after_compression) 

```

Selain itu, kita memiliki fungsi Total_Gain yang mengambil data awal dan kamus berasal dari Calculate_Code yang menyatukan simbol dan kodenya. Fungsi itu menghitung perbedaan antara ukuran bit data terkompresi dan tidak terkompresi.

```python

""" Fungsi pembantu untuk menghitung perbedaan ruang antara data terkompresi dan tidak terkompresi"""    
def Total_Gain(data, coding):
    before_compression = len(data) * 8 # total bit sebelum di kompresi
    after_compression = 0
    symbols = coding.keys()
    for symbol in symbols:
        count = data.count(symbol)
        after_compression += count * len(coding[symbol]) #menghitung berapa banyak bit yang dibutuhkan
    print("Space usage before compression (in bits):", before_compression)    
    print("Space usage after compression (in bits):",  after_compression)           


```
Akhirnya, kita memiliki fungsi Huffman_Encoding yang hanya mengambil data sebagai parameter dan memberi kita hasil pengkodean dan perolehan total menggunakan semua 4 fungsi ini.

Mari kita lihat bagaimana kode bekerja dan keluar untuk kasus kita yang sudah diperiksa!

``` python

def Huffman_Encoding(data):
    symbol_with_probs = Calculate_Probability(data)
    symbols = symbol_with_probs.keys()
    probabilities = symbol_with_probs.values()
    print("symbols: ", symbols)
    print("probabilities: ", probabilities)
    
    nodes = []
    
    # mengubah symbol dan probabilitis kedalam huffman tree nodes 
    for symbol in symbols:
        nodes.append(Node(symbol_with_probs.get(symbol), symbol))
    
    while len(nodes) > 1:
        # mengurutkan semua nodes secara ascending berdasarkan probabilitasnya 
        nodes = sorted(nodes, key=lambda x: x.prob)
        # for node in nodes:  
        #      print(node.symbol, node.prob)
    
        # pick 2 smallest nodes
        right = nodes[0]
        left = nodes[1]
    
        left.code = 0
        right.code = 1
    
        # kombinasikan dua terkecil nodes untuk membuat node baru 
        newNode = Node(left.prob+right.prob, left.symbol+right.symbol, left, right)
    
        nodes.remove(left)
        nodes.remove(right)
        nodes.append(newNode)
            
    huffman_encoding = Calculate_Codes(nodes[0])
    print("symbols with codes", huffman_encoding)
    Total_Gain(data, huffman_encoding)
    encoded_output = Output_Encoded(data,huffman_encoding)
    return encoded_output, nodes[0]  
    
 
def Huffman_Decoding(encoded_data, huffman_tree):
    tree_head = huffman_tree
    decoded_output = []
    for x in encoded_data:
        if x == '1':
            huffman_tree = huffman_tree.right   
        elif x == '0':
            huffman_tree = huffman_tree.left
        try:
            if huffman_tree.left.symbol == None and huffman_tree.right.symbol == None:
                pass
        except AttributeError:
            decoded_output.append(huffman_tree.symbol)
            huffman_tree = tree_head
        
    string = ''.join([str(item) for item in decoded_output])
    return string        


""" Percobaan Pertama """
data = "AAAAAAABCCCCCCDDEEEEE"
print(data)
encoding, tree = Huffman_Encoding(data)
print("Encoded output", encoding)
print("Decoded Output", Huffman_Decoding(encoding,tree))

```

Sebagai kesimpulan, kita melihat bahwa rasio kompresi tidak berubah dengan bertambahnya jumlah data, dan rasio ini mendekati 2:1. Kita dapat mengatakan bahwa Huffman Encoding adalah algoritma yang memampatkan data menjadi ukuran setengahnya. Meskipun sudah tua, ini masih merupakan algoritma kompresi yang efektif!

## 2. Image kompresi menggunakan K-means clustering 

K-means klater akan mengelompokkan warna yang sama menjadi kelompok 'k' (katakanlah k=64) dengan warna berbeda (nilai RGB). Oleh karena itu, setiap pusat cluster mewakili vektor warna dalam ruang warna RGB dari masing-masing cluster. Sekarang, centroid cluster 'k' ini akan menggantikan semua vektor warna di cluster masing-masing. Jadi, kita hanya perlu menyimpan label untuk setiap piksel yang memberi tahu kluster tempat piksel tersebut berada. Selain itu, kita mencatat vektor warna dari setiap pusat klaster.

Library yang dibutuhkan: Kompresi citra menggunakan K-means clustering adalah teknik yang dapat digunakan untuk memperkecil ukuran file citra dengan tetap menjaga kualitas visualnya. Teknik ini melibatkan pengelompokan piksel dalam gambar menjadi beberapa kelompok yang lebih kecil dan kemudian merepresentasikan setiap kelompok dengan warna rata-ratanya. Gambar yang dihasilkan akan memiliki warna yang lebih sedikit, yang mengurangi ukuran file, tetapi keseluruhan tampilan gambar tetap terjaga.

Berikut adalah langkah-langkah untuk mengompresi gambar menggunakan K-means clustering:

1. Mengonversi gambar dari ruang warna aslinya ke ruang warna RGB, yang merepresentasikan warna menggunakan kombinasi nilai merah, hijau, dan biru.

2. Ratakan gambar menjadi larik 2D, di mana setiap baris mewakili satu piksel dan setiap kolom mewakili saluran warna (merah, hijau, atau biru).
3. Terapkan pengelompokan K-means ke array gambar yang diratakan, dengan K mewakili jumlah warna yang diinginkan dalam gambar terkompresi. Algoritme akan mengelompokkan piksel serupa berdasarkan nilai RGB-nya, dan menetapkan nilai RGB rata-rata untuk setiap grup.

4. Ganti setiap piksel pada gambar asli dengan nilai rata-rata RGB dari cluster yang ditetapkan. Ini akan menghasilkan gambar dengan warna yang lebih sedikit, tetapi tampilan keseluruhannya serupa dengan aslinya.
5. Ubah gambar terkompresi kembali ke ruang warna aslinya, jika perlu.
Nah dengan menyesuaikan nilai K, jumlah cluster yang digunakan untuk kompresi, tingkat kompresi bisa dikontrol. Namun, terlalu banyak kompresi dapat mengakibatkan hilangnya detail dan penurunan kualitas gambar. Penting untuk mencapai keseimbangan antara kompresi dan kualitas gambar saat menggunakan teknik ini.

```python 
-> Numpy library: sudo pip3 install numpy. 
-> Matplotlib library: sudo pip3 install matplotlib.
-> scipy library: sudo pip3 install scipy

```
### Kode Pemograman 

```python 
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
# from scipy.io import loadmat
from scipy import misc
 
 
def read_image():
 
    # loading the png image as a 3d matrix
    img = misc.imread('bird_small.png')
 
    # uncomment the below code to view the loaded image
    # plt.imshow(A) # plotting the image
    # plt.show()
 
    # scaling it so that the values are small
    img = img / 255
 
    return img
 
 
def initialize_means(img, clusters):
 
    # reshaping it or flattening it into a 2d matrix
    points = np.reshape(img, (img.shape[0] * img.shape[1],
                              img.shape[2]))
    m, n = points.shape
 
    # clusters is the number of clusters
    # or the number of colors that we choose.
 
    # means is the array of assumed means or centroids.
    means = np.zeros((clusters, n))
 
    # random initialization of means.
    for i in range(clusters):
        rand1 = int(np.random.random(1)*10)
        rand2 = int(np.random.random(1)*8)
        means[i, 0] = points[rand1, 0]
        means[i, 1] = points[rand2, 1]
 
    return points, means
 
 
# Function- To measure the euclidean
# distance (distance formula)
def distance(x1, y1, x2, y2):
 
    dist = np.square(x1 - x2) + np.square(y1 - y2)
    dist = np.sqrt(dist)
 
    return dist
 
 
def k_means(points, means, clusters):
 
    iterations = 10  # the number of iterations
    m, n = points.shape
 
    # these are the index values that
    # correspond to the cluster to
    # which each pixel belongs to.
    index = np.zeros(m)
 
    # k-means algorithm.
    while(iterations & gt
          0):
 
        for j in range(len(points)):
 
            # initialize minimum value to a large value
            minv = 1000
            temp = None
 
            for k in range(clusters):
 
                x1 = points[j, 0]
                y1 = points[j, 1]
                x2 = means[k, 0]
                y2 = means[k, 1]
 
                if(distance(x1, y1, x2, y2) & lt
                   minv):
                    minv = distance(x1, y1, x2, y2)
                    temp = k
                    index[j] = k
 
        for k in range(clusters):
 
            sumx = 0
            sumy = 0
            count = 0
 
            for j in range(len(points)):
 
                if(index[j] == k):
                    sumx += points[j, 0]
                    sumy += points[j, 1]
                    count += 1
 
            if(count == 0):
                count = 1
 
            means[k, 0] = float(sumx / count)
            means[k, 1] = float(sumy / count)
 
        iterations -= 1
 
    return means, index
 
 
def compress_image(means, index, img):
 
    # recovering the compressed image by
    # assigning each pixel to its corresponding centroid.
    centroid = np.array(means)
    recovered = centroid[index.astype(int), :]
 
    # getting back the 3d matrix (row, col, rgb(3))
    recovered = np.reshape(recovered, (img.shape[0], img.shape[1],
                                       img.shape[2]))
 
    # plotting the compressed image.
    plt.imshow(recovered)
    plt.show()
 
    # saving the compressed image.
    misc.imsave('compressed_' + str(clusters) +
                '_colors.png', recovered)
 
 
# Driver Code
if __name__ == '__main__':
 
    img = read_image()
 
    clusters = 16
    clusters = int(
        input('Enter the number of colors in the compressed image. default = 16\n'))
 
    points, means = initialize_means(img, clusters)
    means, index = k_means(points, means, clusters)
    compress_image(means, index, img)

```

## 3. Arithmetic Coding 


Dalam kompresi data, algoritma lossy memampatkan data sambil kehilangan beberapa detail. Mereka mengurangi jumlah bit yang digunakan untuk merepresentasikan pesan, bahkan jika itu mengurangi kualitas data yang direkonstruksi.

Algoritma lossless merekonstruksi data asli tanpa kehilangan apapun. Karena itu, mereka menggunakan jumlah bit yang lebih tinggi dibandingkan dengan algoritma lossy.

Encoding aritmatika (AE) adalah algoritma lossless yang menggunakan jumlah bit yang rendah untuk mengompres data.

Ini adalah algoritme berbasis entropi, pertama kali diusulkan dalam makalah dari tahun 1987 (Witten, Ian H., Radford M. Neal, dan John G. Cleary. "Pengkodean aritmatika untuk kompresi data." Komunikasi ACM 30.6 (1987): 520 -540).

Salah satu alasan mengapa algoritme AE mengurangi jumlah bit adalah karena AE mengkodekan seluruh pesan menggunakan satu angka antara 0,0 dan 1,0. Setiap simbol dalam pesan mengambil sub-interval dalam interval 0-1, sesuai dengan probabilitasnya.

Untuk menghitung probabilitas setiap simbol, tabel frekuensi harus diberikan sebagai masukan ke algoritma. Tabel ini memetakan setiap karakter ke frekuensinya.

Semakin sering simbolnya, semakin rendah jumlah bit yang diberikan. Akibatnya, jumlah bit yang mewakili seluruh pesan berkurang. Ini dibandingkan dengan menggunakan jumlah bit yang tetap untuk semua simbol dalam pesan seperti dalam pengkodean Huffman.

```python

import string
import random
from collections import Counter
import time

# Arithmetic Encoding
def ac_encode(txt):

    res = Counter(txt)

    # characters
    chars = list(res.keys())

    # frequency of characters
    freq = list(res.values())

    probability = []
    for i in freq:
        probability.append(i / len(txt))

    print(chars)
    print(probability)

    high = 1.0
    low = 0.0
    for c in txt:
        diff = high - low
        index = chars.index(c)
        for i in range(index):
            high = low + diff * probability[i]
            low = high

        high = low + diff * probability[index]
        print(f'char {c} -> Low: {low}   High: {high}')

    tag = (low+high)/2.0

    print('Input: ' + txt)
    print(str(low) + '< codeword <' + str(high))
    print('codeword = ' + str(tag))

    with open('encode.ac', 'w') as fw:
        for i in chars:
            fw.write(i + ' ')
        fw.write('\n')

        for i in probability:
            fw.write(str(i) + ' ')
        fw.write('\n')

        fw.write(str(tag))

    return chars, probability, tag


# Arithmetic Decoding
def ac_decode(chars, probability, tag):
    high = 1.0
    low = 0.0
    output = ''
    c = ''
    while (c != '$'):
        diff = high - low
        for i in range(len(chars)):
            high = low + diff * probability[i]
            if low < tag < high:
                break
            else:
                low = high

        c = chars[i]
        output += c

    return output


def arithmetic_coding(input):
    if '$' in input:
        input = input[0:input.index('$')]
    if input[-1] != '$':
        input += '$'

    print('Input: ' + input)

    start = time.time()
    (chars, probability, tag) = ac_encode(input)
    output = ac_decode(chars, probability, tag)
    end = time.time()

    print('Decode: ' + output)

    print('does match :  ' + str(input == output))
    print(f"Total Time: {end - start} sec\n\n")
    return input == output


############# INPUT ######################
# Random String , 100 test case
count = 0
testcase = 10
for i in range(testcase):
    # generating string
    letters = string.ascii_uppercase
    random_txt = ''.join(random.choice(letters) for i in range(13)) + '$'
    flag = arithmetic_coding(random_txt)
    if flag:
        count += 1

print(f"Total Test: {testcase}")
print(f"Succecss: {count}")


#----------------------------------------
# User given specific data
# Please use small string (less than 13 characters)
txt = "BANGLADESH$"
arithmetic_coding(txt)


##########################################

```

# Challenge
Implementasikan berbagai algoritma image compression yang ada, Huffman Encoding, K-means Clustering, dan Arithmetic Coding, dengan contoh data masing2


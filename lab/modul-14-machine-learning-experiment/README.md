# Eksperimen, Pengembangan Produk, dan Publikasi Machine Learning Multimedia

## Memberikan Solusi Untuk Permasalahan di Dunia Sebagai Bentuk Ibadah
1. Solusi dari masalah yang mengancam keselamatan umat manusia
    - Bencana
    - Penyakit, wabah, pandemi, dsb.
    - Kesehatan fisik, mental, spiritual
    - Kecelakaan
    - Kriminalitas
    - Komunikasi
2. Solusi yang memaksimalkan profit untuk industri
    - Solusi untuk UMKM
    - Optimasi penggunaan sumber daya di industri
    - Inovasi pembuatan dan pendistribusian produk
    - Rekomendasi kebijakan yang lebih menguntungkan
3. Solusi dari masalah-masalah yang lebih spesifik
    - Membuat bot auto menang game Mobile Legend
## Cross Industry Standard Process for Data Mining
```mermaid
flowchart TD
    A(1. Business Understanding) <--> B(2. Data Understanding)
    B --> C(3. Data Preperation)
    C <--> D(4. Modeling)
    D --> E(5. Evaluation)
    E --> A
    E --> F(6. Deployment)
```

### 1. Business Understanding
- Latar belakang yang kuat
    - Contoh: 
        - Mengetikkan instruksi kepada komputer membutuhkan waktu yang lama ketika perintahnya kompleks
        - Tidak setiap kesempatan seseorang dapat mengetik instruksi kepada komputer, misalnya ketika beraktifitas
- Usulan solusi yang jelas
    - Contoh:
        - Membuat model Speech to Text yang dapat mengkonversi perintah berupa suara menjadi teks
        - Model Speech to Text dapat dibuat melalui pendekatan machine Learning:
            - Model dilatih dengan menggunakan data suara dari pengucapan kata yang dilabeli tulisan kata nya
- Tujuan yang jelas
    - Contoh:
        - Membangun model Speech to Text Bahasa Indonesia
        - Meningkatkan kinerja dari model Speech to Text Bahasa Indonesia

### 2. Data Understanding
- Memahami bagaimana data digunakan untuk mencapai Tujuan
    - Contoh:
        - Untuk membuat model Speech to Text dibutuhkan dataset pengucapan kata
    - Menjawab pertanyaan-pertanyaan seperti:
        - Dataset seperti apa yang dapat digunakan ?
        - Berapa jumlahnya ?
        - Bagaimana karakteristik dan sebaran datanya ?
        - Dari mana data tersebut akan didapatkan ?
- Exploratory Data Analysis
    - Berbagai teknik statistik dan visualisasi data untuk memahami karakteristik data
    - Umumnya dilakukan menggunakan tools analisis dan visualisasi interaktif berbasis script Python / R
    - Contoh tool:
        - Jupyter Notebook
            - Google Colab
        - Apache Zeppelin
    - Contoh library:
        - Python
            - Pandas (eksplorasi dan query data)
            - Matplotlib / Seaborn (visualisasi data)

### 3. Data Preparation
- Melakukan berbagai prosedur penyediaan data hingga siap digunakan pada pemodelan
    - Contoh:
        - Mencari dan menentukan sumber-sumber data yang dapat digunakan
        - Menentukan cara pengambilan data, misal:
            - Bersurat dan isi form dari pemilik data
            - Data crawling dan scraping
            - Melakukan pengambilan data secara langsung seperti perekaman suara
            - Mengambil data input dari user melalui database ataupun storage
        - Melakukan pemrosesan untuk normalisasi format dan isi data
        - Melakukan pelabelan data
        - Melengkapi atribut-atribut yang tidak lengkap
        - Memastikan jumlah dari tiap label seimbang, atau melakukan penanganan jika tidak seimbang
- Menggunakan berbagai tools dan library untuk pemrosesan data seperti:
    - Python
        - Pandas (eksplorasi dan query data)
        - Scikit-learn (berbagai tool machine learning)
        - Augmentasi dan pemrosesan image:
            - Scikit-image
            - OpenCV
            - Torchvision

### 4. Modeling
- Menggunakan data hasil dari Data Preparation untuk melatih model melalui metode machine Learning
- Melakukan validasi model sekaligus menentukan konfigurasi terbaik model yang memaksimalkan kinerja dari model
- Pada tahap ini didapatkan konfigurasi terbaik yang memaksimalkan kinerja model
- Menggunakan berbagai tools dan library machine learning / deep learning seperti:
    - Python
        - Keras (deep learning)
        - PyTorch (deep learning)
        - Tensorflow (deep learning)

### 5. Evaluation
- Menguji model terbaik dengan data tes
- Mendapatkan kinerja hasil pengujian model terbaik

### 6. Deployment
- Model yang telah dibuat, diintegrasikan dengan aplikasi yang telah ada, atau aplikasi yang sedang dibuat
- Contoh:
    - Model Speech to Text dipasang pada aplikasi Asisten Pribadi berbasis Smartphone
    - Model pengenal penyakit tumbuhan berbasis foto dipasang pada aplikasi Smartphone
    - Model pengenal wajah orang dipasang pada web service

## Penulisan Paper
### Struktur Umum
- **Abstract**
- **Introduction**
- Body
    - Literature Review / **Related Works**
    - Problems / Problems breakdown (Problem A, Problem B, Problem C, etc.)
    - **Methods** / Methods breakdown (Method A, Method B, Method C, etc.)
    - Experiments / **Results**
    - **Discussions** / Future Works
- **Conclusion**
- **References**

### Artikel (Paper) Rujukan
- [Jurnal terbaik sedunia terkait Artificial Intelligence - Scimagojr](https://www.scimagojr.com/journalrank.php?category=1702)
- Dari jurnal ataupun conference yang bereputasi secara internasional, bisa cari via:
    - [Institute of Electrical and Electronics Engineers (IEEE) - IEEEExplore](https://ieeexplore.ieee.org/Xplore/home.jsp)
    - [Association for Computing Machinery (ACM) - ACM DL Library](https://dl.acm.org/)
    - [ScienceDirect](https://www.sciencedirect.com/)
    - [arxiv](https://arxiv.org/)
    - [Jurnal Online Informatika UIN Sunan Gunung Djati Bandung](https://join.if.uinsgd.ac.id/index.php/join/search)

### Publikasi Artikel
- Publikasi via:
    - Journal
    - Conference

### Template
- IEEE 
    - [Format Word](https://www.ieee.org/content/dam/ieee-org/ieee/web/org/conferences/conference-template-a4.docx)

## Referensi
- [Data Science Methodology From Understanding to Preparation](https://medium.com/ml-research-lab/part-3-data-science-methodology-from-understanding-to-preparation-a666a8203179)

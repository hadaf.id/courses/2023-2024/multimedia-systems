# Audio Compression

## Script
[Google Colab](https://colab.research.google.com/drive/1K_3C9qUhMYGdJY1IfbG7eUo9z_LygVla#scrollTo=udhADBGdIYag)

## Referensi
### Konsep
- [Audio Codecs Explained for Non-Audiophiles](https://www.audioholics.com/audio-technologies/codecs)
- [Audio compression explained - Soundguys](https://www.soundguys.com/audio-compression-explained-29148/)
- [Advanced Audio Coding (AAC) Open Edu](https://www.open.edu/openlearn/science-maths-technology/exploring-communications-technology/content-section-3.7)
- [Advanced Audio Coding - Techopedia](https://www.techopedia.com/definition/218/advanced-audio-coding-aac)

### Tools
- [ffmpeg](https://ffmpeg.org/ffmpeg.html)
- [ffmpeg-python](https://github.com/kkroening/ffmpeg-python)

## Challenge
- Melanjutkan penggabungan Audio ditambahkan dengan kompresi audionya

# Video Compression

## Menggunakan FFmpeg

```python
import subprocess

bitrate = '800k'
input_file = 'mancing-ikan.mp4'
output_file = 'mancing-ikan-compressed.mp4'

subprocess.call("ffmpeg -i {} -b {} {}".format(
    input_file,
    bitrate,
    output_file
))

```

# Image Processing Tools

## Tools
Lisensi | Platform | Tipe Grafik | Merk |Tampilan| Penggunaan | Download
---|---|---|---|---|---|---|
Open Source 🌟 | Desktop GUI |Vector | Inkscape| ![Inkscape](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/inkscape1.png)| Desain vektor poster, web UI, icon vektor, medsos | [🔗Download](https://inkscape.org/release/inkscape-1.2.2/) [🔗Source code](https://gitlab.com/inkscape/inkscape)
Open Source 🌟 | Desktop GUI | Raster | Gimp | ![Gimp](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/gimp1.png)| Editing foto, poster, medsos | [🔗Download](https://www.gimp.org/downloads/) [🔗Source code](https://github.com/GNOME/gimp)
Open Source 🌟 | Web GUI | Vector | Penpot | ![Penpot](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/penpot1.png)| Desain UX UI interaktif, icon vektor | [🔗Download](https://www.gimp.org/downloads/) [🔗Source code](https://github.com/GNOME/gimp)
Open Source 🌟 | CLI | Raster | ImageMagick |![ImageMagick](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/ImageMagick_display_7.0.11-4_screenshot.png)| Otomasi pemrosesan image, web backend image | [🔗Download](https://imagemagick.org/script/download.php) [🔗Source code](https://github.com/imagemagick/imagemagick)
Open Source 🌟 | Desktop GUI | Vector | Blender |![Blender](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/blender.jpg)| Model 3d, aset game 3d, seni 3d, simulasi | [🔗Download](https://www.blender.org/download/) [🔗Source code](https://github.com/blender/blender)
Free trial atau Subscription 🌟 | Desktop dan Web GUI |Vector |canva| ![Canva](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/Canva.png)| Desain poster, Flyer, presentaisi template, web UI, icon,| [🔗Download](https://www.canva.com/en_gb/)
Free trial atau Subscription 🌟 | Desktop dan Web GUI |Vector |Figma| | Desain UX UI interaktif, icon, poster| [🔗Pakai](https://www.figma.com)
Free trial atau Subscription 🌟 | Dekstop dan Web GUI | Vector | Adobe Photoshop | ![Adobe](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/adobe_photoshop.png)|Desain vektor, editing foto, ilustrasi, poster, Flyer, icon vektor,|[🔗Download](https://www.adobe.com/id_en/products/photoshop/landpa.html?sdid=85665QDS&mv=search&ef_id=Cj0KCQjww4-hBhCtARIsAC9gR3aSSxNDnZd32WhrZLZXifaQa9tCHD6eN9ZNbkAVNDQHdVUo6O5Sp5QaAkesEALw_wcB:G:s&s_kwcid=AL!3085!3!444512448900!e!!g!!adobe%20photoshop!703952628!38400836578&gclid=Cj0KCQjww4-hBhCtARIsAC9gR3aSSxNDnZd32WhrZLZXifaQa9tCHD6eN9ZNbkAVNDQHdVUo6O5Sp5QaAkesEALw_wcB)
Free trial atau Subscription 🌟 | Dekstop GUI, Mobile GUI Android, Mac, Windows OS| Raster| Medibangpaint|![Medibangpaint](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/medibangpaint.png)  |Ilustrasi, webtoon atau komik drawing, Desain poster| [🔗Download](https://medibangpaint.com/en/app-download/)
Free trial atau Subscription 🌟 | Dekstop GUI, Mobile GUI Android, Mac, Windows OS| Raster | Ibistpaint | ![Ibistpaint](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/ibispaint.png)| Ilustrasi, webtoon atau komik drawing, Desain poster| [🔗Download](https://ibispaint.com/product.jsp)
Free trial atau Subscription 🌟 | Mobile GUI Android, Mac | Vector | Infinite paint | ![Infinite Painter](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/infinitepainter.png)| Ilustrasi, webtoon atau komik drawing, Desain poster, icon|[🔗Download](https://www.infinitestudio.art/discover.php)
Free trial atau Subscription 🌟 | Mobile GUI Mac OS | Vector |ProCreate| ![ProCreate](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/Procreate.png)|Ilustrasi, Desain vektor poster, drawing, Video, animasi |[🔗Download](https://procreate.com)
Free trial atau Subscription 🌟 | Desktop GUI, Mobile GUI Android, Mac, Windows OS| Vector |Concept| ![Concept](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/concepr.png)| Ilustrasi, drawing, icon, komik drawing|[🔗Download](https://concepts.app/en/)
Free atau Paid 🌟 | Web UI / Web API | Raster |Remove.bg| | Menghilangkan background|[🔗Download](https://www.remove.bg/)
Free atau Paid 🌟 | Web UI / Web API | Raster |OpenAI DALL-E| | Membuat image dari deskripsi teks, menggabungkan image dengan image lain|[🔗Pakai](https://labs.openai.com/)




## Development Library

Output profesi : User, Scientist, Developer (engginer)

Lisensi | Platform | Tipe Grafik | Merk |Tampilan| Penggunaan | Download
---|---|---|---|---|---|---|
Open Source 🌟 | WebGUI |Vector | Webgl-html5|![webgl](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/webGL_HTML.png)| web development, drawing, color, design page 2D Canvas, WebGL, SVG, 3D CSS transforms, dan SMIL| [🔗Referensi dan source code](https://www.tutorialspoint.com/webgl/html5_canvas_overview.htm)
Open Source 🌟 | Mobile app android |Vector | Android SBK|![simpedrawing](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/hello_world.png)| drawing tools, color, design page | [🔗Referensi dan source code](https://guides.codepath.com/android/Basic-Painting-with-Views)
Open Source 🌟 | Aplikasi mobile dan dekstop GUI | Vector | Cairographics |![C++](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-4-image-processing-tools/cairographic.png)| drawing tools, color, ilustrasi basic image editing, gradient, vector dan 3D  | [🔗Referensi](https://www.cairographics.org/OpenGL/) [🔗Source code](https://www.cairographics.org/tutorial/#L1draw)


## Referensi
- [Best free open source image processing library - Linuxlinks](https://www.linuxlinks.com/best-free-open-source-image-processing-libraries/)

## Challenge
1. Buat poster / infografik / webtoon dengan tools gui image dengan tema (pilih):
    - Teknologi masa depan
    - 10 tahun lagi kamu sedang jadi apa

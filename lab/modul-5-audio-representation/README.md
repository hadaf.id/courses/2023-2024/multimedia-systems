# Module 5 - Audio Representation

## Dasar Audio Processing
Dalam matakuliah Fisika Dasar fenomena audio/suara ini erat kaitan dengan bentuk gelombang dengan beberapa proses audio/suara ini bisa di tangkap oleh telinga manusia karen proses gelombang yang merambat pasa sebuah medium yaotu udara. Beberapa parameter dari gelombang ini dinyatakan salah satunya sebagai dasar untuk mengenal Audio representaion, salah satu parameternya ialah frekuensi yang menyatakan banyaknya gelombang per detik yang di nyatakan dalam satuan (Hz), dikenalkan juga Periode (T) dimana secara matematis ini merupakan invers dari frekuensi menunjuan lama waktu untuk sebuah gelombang, dan istilah lainya seperti panjang gelombag ($\lambda$), Amplitudo ($A$).

![Gelombang](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-5-audio-representation/Gelombang.png)


Audio ini direpresentasikan sebagai bentuk gelombang yang memiliki persamaan gelombang dengan bentuk sinyal sinus atau cosinus : 
$$ y = A \sin (2 \pi f t + \theta) $$
dimana : 
- A = Amplitudo 
- f = frekuensi 
- t = eaktu dan detik 
- $\theta$ = sudut fase dari sinyal 

## Pengaruh Perubahan Amplitudo

pengaruh dari proses ini berkaitan dengan kuat bunyi(intesitas Bunyi), keras atau lemahnya bunyi yang terdengar bergantung terhadap amplitufo semakin besar amplitudo getaran sumber bunyi, semakin keras bunyi yang dihasilkan dan sebaliknya

## Pengaruh Perubahan frekuensi

Pengaruh yang paling signifikan dari perubahan frekuensi ialah beriaktan dengan kualitas suara itu sendiri. beberapa isitlah ini dikenal dengan hukum Marseme yang berkaitan dengan faktor frekunsi alamiah sebuah senar, dawai, tegangan senar, masa jenis senar. 

## Pengaruh Perubahan Fasa 

Gelombang suara dalam fasa yang dijumlahkan akan menghasilkan gelombang yang lebih kuat, Gelombang ini bisa saling menjumlahkan mengahasilkan no saay berlawan, dan akan punya fasa yang bervariasi itulah yang menghasilkan pengaruh suara berbeda. 

![sudut fasa](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-5-audio-representation/gelombang_sudut_fase.png)

Pengaruh pada fasa ini biasanya digunakan pada proses noice cancelling pada headphone didalamnya memiliki microphone yang menangkap noise dari luar dan kemudia direproduksi pada speaker headphone secara out of phase, oleh karena itu suara noise dari luar akan bertabrakan dengan noise dari dalam secara berlawanan untuk menghilangkan suara noise dari telinga

## Sinyal Audio

Pada prinsipnya sinya audio sederhana dinyatakan dalam sinya sinusoidal, semua operasi atau proses yang berlaku pada sebuah sinyal sinusosidal akan dioperasikan pada sinyal audio. Terdapat proses konversi dari audio analog ke audio digital yang prinsipnya sama dengan proses ADC pada sinyal analog. 

## Spectogram 

![spectogram](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-5-audio-representation/spectogram.png)

Spectogram merupakan diagram yang digunakan untuk melihat suatu gelombang dalam periode waktu yang pendek, akusisi data suara akan lebih baik diamatai menggunakan spectogram. Data suara ini di analisis yang paling mudah berbentuk grafik dengan dua dimensi geometris yang submi horizontal mewakili waktu dan sumbu vertika mewakili frekuensi, dimensi lain yaitu amplitufo frekeusni tertentu pdengan intensitas atau warna titik yang di representasikan.

## Bentuk Gelombang dan Jenisnya 

Electrical Waveform merupakan Bentuk Gelombang Listrik merupakan bagian dari Gelombang Elektromagnetik yang tidak memiliki bentuk fisik. Osiloskop digunakan untuk mevisualisasikan gelombang/sinyal. Selain itu, kita juga dapat menggambarkannya ke kertas grafik dengan menghubung setiap perubahan titik plot tegangan pada suatu periode waktu tertentu. Secara Teknik, Bentuk Gelombang Listrik (Electrical Waveform) ini adalah representasi visual dari perubahaan tegangan atau arus terhadap waktu.

[Bnetuk Gelombang](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-5-audio-representation/waveform.png)

- Klasifikasi Bentuk Gelombang Listrik (Electrical Waveform)
    - Uni-directional Waveform : bentuk gelombang yang selalu berada pada wilayah Positif ataupun Negatif dan tidak ada akan memotong titik sumbu nol.  Contoh Uni-directional Waveform ini adalah Square Wave Waveform (Bentuk Gelombang Kotak) untuk Sinyal Pewaktu (Timing), Sinyal Pulsa Jam (Clock Pulse) dan Sinyal Pemicu (Trigger).
    - Bi-directional Waveform : bentuk gelombang yang bergantian bolak-balik dari Positif ke Negatif dan Negatif ke Positif dengan melintasi titik sumbu Nol. Oleh karena itu, Bi-directional Waveform sering disebut dengan bentuk gelombang bolak-balik (Alternating Waveform). Bentuk Gelombang Bi-directional Waveform yang paling umum adalah Bentuk Gelombang Sinus (Sine Waveform).
    
- Jenis Bentuk Gelombang Listrik (Electrical Waveform)
    - Bentuk Gelombang Sinus 
        Sine Waveform atau juga disebut dengan Sinusoida Waveform adalah salah satu bentuk gelombang yang paling umum ditemukan di rangkaian Elektronika terutama pada sinyal-sinyal Analog seperti sinyal Audio, sinyal tegangan AC dan sinyal RF (gambar a)
    - Bentuk Gelombang Kotak (Square Waveform)
        Square Waveform ini memiliki bentuk seperti Kotak dan umumnya digunakan pada rangkaian mikro elektronik untuk pengendalian waktu (timing control). Hal ini dikarenakan Square Waveform memiliki bentuk gelombang simetris dengan durasi yang sama pada siklus setengah kotak dengan setengah kotak lainnya (memiliki interval yang teratur).(gambar b)
    - Bentuk Gelombang Persegi (Rectangular Waveform)
        Memiliki bentuk yang hampir sama dengan bentuk gelombang kotak. Namun Interval waktu kondisi High dan Low tidak teratur atau tidak memiliki panjang waktu yang sama (gambar c)
    - Bentuk Gelombang Gigi Gergaji (Saw Tooth Waveform)
        Bentuk Gelombang Gigi Gergaji atau Saw Tooth Waveform adalah gelombang yang berbentuk seperti gigi gergaji. Pada Bentuk Gelombang Gigi Gergaji ini, tegangan naik secara linear dari titik 0 hinggi titik mencapai titik tertinggi (+V) kemudian jatuh secara tiba-tiba ke titik terendahnya (0) tanpa atau pewaktuan. Gelombang Gigi Gergaji ini digunakan pada Rangkaian Televisi terutama pada TV yang masih menggunakan tabung CRT dan juga sebagai Pemicu (Trigger) pada rangkaian Digital (gambar c)
    - Bentuk Gelombang Segitiga (Triangular Waveform)
        Bentuk Gelombang Segitiga adalah Gelombang yang berbentuk Segitiga. Tegangan naik secara linear dari Nol (0V) hingga mencapai titik tertingginya (+V). Tegangan Tertinggi tersebut hanya bertahan pada waktu yang sangat singkat pada puncaknya (berbentuk lancip) kemudian turun secara linear hingga mencapai titik terendahnya (-V). Di titik terendah, tegangan tersebut juga berada dalam waktu yang sangat singkat sekali sehingga membentuk kurva lancip. (gambar d)


## Skema Pemrosesan Audio Digital 

![Skema Pemrosesan Audio Digital ](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-5-audio-representation/audio_digital_proses.png)

## Decibel 
Decibel(dB) menyatakan intesitas suara yang di tunjukan menggunakan persamaan berikut : 

$$ X_{dB} = 10log_{10}(\frac{X}{X_0}) $$

$X_0$ = nilai awal, Decibel menyatakan intersitas suara relatif terdap 0 dB, Referensi 0 Db mewakili batas pendengaran manusia. 

## Parameter Sinyal Audio Dasar 

- Sampling Frequency 
Banyaknya jumpah sample yang diambil dalam satuan waktu (detik) dari signal yang diteri bentuk signal kontinu menjadi sinya yang terpisah (signal diskrit). nyquist menyatakan Fs > dua kali dari frekuensi maksimal contoh : Fs >= 40 kHz , CD audio ( 44.1 kHz) Blue-ray (48 kHz)
- Bit Depth 
Ukuran data digital yang didapatkan dari hasil konversi analog ke digital besarnya 16 Bit dan 24 Bit.
    - Bit rate merupakan kecepatan data yang di ukur dengan waktu seperti Kbps, Mpbs dst. 

        Bit Depth ini akan berpengaruh pada tingkat intensitas atau tingkat ebisingan terhadap pendengaran manusia, sebagai contoh :
        - threshold of hearing - 0 dB 
        - Mesin Jet - 100 s.d 140 dB
        - Busy Road - 100 dB
        Semakin tinggi bit depth, maka gelombang sinyal audio akan berkuran dan S/N akan meningkat dan sebaliknya. S/N semakin tinggi maka kualitasnya semakin bagus. Bit depth juga digunakan untuk mentukan dinamic range (perbedaan selisih) natara signal tertinggi dan signal teredah yang di rekan ataupun diolah. 
- Raw Data Rate 
Data rate merupakan ukuran kecepatan bit data dalam sebuah proses transmisi, dhitung dalan bit perdetik, dihitung berdasarkan frekuseni sampling (bit depth). Contoh : Berapa raw data rate audio moni berduari 2 menit dengan standar Blue-ray (48 kHz) dan diketahui bit depth sebesar 16 bits persample. 
maka : $1 \times 120 s \times 48000 Hz \times 16 bps = 92 Mbits/second$

## Format File Audio 

![format file](https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-5-audio-representation/fprmat_data_.png)

1. MPEG layer 3 (MP3)

Mp3 merupakan format kompresi audio yang dikembangkan oleh Moving Picture Experts Group (MPEG). Format file ini menggunakan Layer 3 kompresi audio yang secara umum digunakan untuk menyimpan file – file music dan audiobooks dalam hard drive. Format file mp3 mampu memberikan kualitas suara yang mendekati kualitas CD stereo dengan 16-bit. MP3 mengalami kejayaan pada tahun 1995, dimana semakin banyak file MP3 tersedia di internet dan popularitasnya semakin terdongkrak karena kualitasnya dan kapasitas yang menjadi relatif sangat kecil.

Kualitas suara file MP3 tergantung pada sebagian besar bit rate yang digunakan untuk kompresi. Bit rate yang sering digunakan biasanya berkisar antara 128, 160, 192, 256 dan 320 kbps. Semakin besar bit rate, semakin bagus kualitasnya, namun hal tersebut berpengaruh pada kebutuhan ruang dalam disk yang semakin besar pula. Untuk mendapati kualitas yang mendekati kualitas CD diperlukan bit-rate 320 kbps.

2. WAV (Wave-form)

WAV adalah singkatan dari “waveform” yang merupakan standar format berkas audio yang dikembangkan oleh Microsoft dan IBM. WAV ini adalah format utama untuk menyimpan data audio mentah pada Windows dan menggunakan metode yang sama dengan AIFF Apple untuk menyimpan data. WAV umumnya digunakan untuk menyimpan audio tanpa kompresi, file

suara berkualitas CD. File ini berukuran besar, sekitar 10 MB per menit. File wav juga dapat berisi data terkodekan dengan beraneka ragam codec untuk mengurangi ukuran file. Akan tetapi untuk keperluan mengoleksi musik, transfer via internet dan memainkan di player portable, format ini kurang popular dibandingkan dengan MP3, Ogg Vorbis dan WMA dikarenakan ukuran file yang sangat besar. Format extensinya yaitu: .wav atau .wv

3. AAC (Advance Audio Coding)

AAC adalah file format audio yang berbasis MPEG2 dan MPEG4. AAC bersifat lossy compression (data hasil kompresi tidak bisa dikembalikan lagi ke data semula, karena setelah dikompres terdapat data-data yang hilang). File AAC dikembangkan oleh Motion Picture Expert Group (Fraunhofer Institute, Dolby, Sony, Nokia dan AT&T). File AAC dikompresi dengan cara lebih efisien pada kecepatan 128 kbps dengan suara stereo dibandingkan versi yang lebih dulu muncul, yakni, MP3. AAC merupakan audio codec yang menyempurnakan MP3 dalam hal medium dan high bit rates. Format extensinya yaitu: .m4a, .m4b, .m4p, .m4v, .m4r, .3gp, .mp4, .aac

4. WMA

WMA (Windows Media Audio) adalah format yang ditawarkan oleh Microsoft. Format ini di desain khusus untuk digunakan pada Windows Media Player yang ada pada sistem operasi windows. Kelebihan Format WMA yaitu bisa dijalankan pada media player lain juga walaupun berada pada sistem operasi yang lain. Selain itu WMA juga sangat disukai vendor musik online karena dukungannya terhadap Digital Rights Management (DRM). DRM (Digital Rights Management ) adalah fitur yang mendukung pencegahan terhadap pembajakan musik. Kualitas musiknya pun lebih bagus dibandingkan MP3 dan AAC. Akan tetapi dibalik kelebihannya itu ada juga kekurangannya, diantaranya yaitu file WMA memiliki ukuran yang cukup besar karena teknik kompresi kurang dilakukan dengan maksimal, dan sangat jarang digunakan di internet karena ukuran filenya yang sangat besar.

5. MP4

MP4 adalah singkatan dari MPEG-4 Part 14. MP4 bukanlah penerus dari MP3. Sebaliknya, MP4 adalah format file mandiri berdasarkan format QuickTime MOV-Basic Apple. MP4 adalah format file yang dapat menyimpan audio, video, gambar, teks. Anda dapat menggunakannya untuk menyimpan semua jenis data dengan kualitas yang sangat baik.

Saat menyimpan data dalam format MP4, Anda dapat memilih dari beberapa codec audio yang berbeda. Misalnya, ini mendukung AAC (Advanced Audio Codec) dan ALAC (Apple Lossless Audio Codec). MP4 tentu merupakan peningkatan dari MP3, tetapi memiliki dasar yang sangat berbeda. Namun, fungsi teknisnya tetap sama.

## Kode di Google colab
[Lets, Play the Game](https://colab.research.google.com/drive/1XfTK8klrcMvSjfUBOOq0NIrvmCuVAfVm?usp=sharing)

## Challenge 

Buatlah audio gabungan antara musik, suara kamu sendiri, dan suara parodi di internet

## Referensi

### Library
- [Librosa - Music and audio analysis](https://librosa.org/doc/latest/index.html)
- [Soundfile - Audio library based on libsndfile, CFFI and NumPy](https://pysoundfile.readthedocs.io/en/latest/)
- [Pydub - Manipulate audio with a simple and easy high level interface](https://github.com/jiaaro/pydub/)
- [ThinkDSP - Digital Signal Processing in Python](https://github.com/AllenDowney/ThinkDSP)
- [gtts - library and CLI tool to interface with Google Translate’s text-to-speech API](https://gtts.readthedocs.io/en/latest/)
### Tool
- [Audacity - Free, open source, cross-platform audio software](https://www.audacityteam.org/download/)
- [LMMS - Free, open source, multiplatform digital audio workstation.](https://lmms.io/)
### Artikel Terkait
- [Audio Data Analysis using Python](https://www.topcoder.com/thrive/articles/audio-data-analysis-using-python)
- [Audio Data Analysis Deep Learning Python part 1 - KDNuggets](https://www.kdnuggets.com/2020/02/audio-data-analysis-deep-learning-python-part-1.html)
- [Beginner's guide to audio data-FizzBuzz](https://www.kaggle.com/code/fizzbuzz/beginner-s-guide-to-audio-data)
- [Know your audio signal processing techniques - Videomaker](https://www.videomaker.com/article/c04/18241-know-your-audio-signal-processing-techniques/)
- [Audio Processing in Python - Tyler Hilbert](https://colab.research.google.com/github/Tyler-Hilbert/AudioProcessingInPythonWorkshop/blob/master/AudioProcessingInPython.ipynb)
- [How to Create Text to Speech From - Paparadit](https://paparadit.blogspot.com/2021/10/how-to-create-text-to-speech-from.html)

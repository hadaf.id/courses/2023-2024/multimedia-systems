# Image Representation

## Apa itu OpenCV ?

- Library open source
- untuk pemrosesan image dan video
- dapat digunakan dari berbagai bahasa pemrograman (Python, C++, Java)
- dapat diintegrasikan dan dimaksimalkan fungsionalitasnya dengan library operasi numberik seperti Numpy

## Tutorial
[![Aneka transformasi Image dengan OpenCV menggunakan Python](https://img.youtube.com/vi/vIVXuFtpjnA/0.jpg)](https://www.youtube.com/watch?v=vIVXuFtpjnA)

## Instalasi dan Inisiasi Package di Python Google Colab

### Instalasi
```sh
!pip install opencv-python
```

### Inisiasi
```py
# Mengimport library OpenCV
import cv2

# Mengimport cv2_imshow untuk menampilkan image di Jupyter Lab
# cv2_imshow merupakan perbaikan dari cv2.imshow 
# yang tidak dapat digunakan di Google Colab
from google.colab.patches import cv2_imshow

# Mengimport numpy, library untuk pemrosesan numerik berbasis matriks
import numpy as np

# Mengimport random, library untuk membuat nilai angka secara acak
import random
```

## Mengakses Image

### Membaca Image
```py
# Membaca image dari file dino.png
# representasi dari image tersebut disimpan di variabel image_dino
# Download terlebih dahulu contoh image dari 
# https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/raw/main/modul-2/dino.png
# Lalu upload ke folder sample_data di Google Drive
alamat_image = '/content/sample_data/dino.png'

image_dino = cv2.imread(alamat_image)

# nama_image.shape berisi tuple dimensi (tinggi, lebar, jumlah_channel)
dimensi = image_dino.shape
tinggi = dimensi[0]
lebar = dimensi[1]
jumlah_channel = dimensi[2]

print("Image {} tingginya {} lebarnya {}".format(alamat_image, tinggi, lebar))
```

### Membaca kode warna Blue, Green, Red (BGR) dari image
```py
# Mengambil nilai B, G, R satu pixel pada lokasi 7,0
baris_ke = 7
kolom_ke = 0
(B, G, R) = image_dino[baris_ke, kolom_ke]
print(B, G, R)
```

### Menampilkan image di Google Colab
```py
cv2_imshow(image_dino)
```

## Manipulasi Bentuk, Posisi dan Ukuran Image

### Memotong Gambar / Cropping
```py
x1 = 100 # Titik 0 dari x adalah bagian paling kiri image
y1 = 0 # Titik 0 dari y adalah bagian paling atas image
x2 = 700
y2 = 450

# Akses komponen array dari image untuk melakukan cropping
image_dino_crop = image_dino[y1:y2, x1:x2]

# Menampilkan hasil cropping
cv2_imshow(image_dino_crop)
```

### Mengubah Ukuran / Resize dengan Skala
> Resize tanpa rasio yang tepat kan membuat image terlihat pipih / gepeng
```py
minimum_lebar = 800
minimum_tinggi = 1500

skala_lebar = minimum_lebar / image_dino_crop.shape[1]
skala_tinggi = minimum_tinggi / image_dino_crop.shape[0]
skala = min(skala_lebar, skala_tinggi) # Skala perubahan ukuran

dimensi_image_resize = (int(image_dino_crop.shape[1] * skala), int(image_dino_crop.shape[0] * skala))

image_dino_resize = cv2.resize(
  image_dino_crop,
  dimensi_image_resize
)
cv2_imshow(image_dino_resize)
```

### Merotasikan Image
```py
panjang_image = image_dino_crop.shape[1]
tinggi_image = image_dino_crop.shape[0]

# Sudut rotasi menggunakan fungsi random
# agar bisa merotasi secara acak pada rentang yang ditentukan
# Rotasi acak umum digunakan pada proses image preprocessing maching learning
# Untuk rotasi dengan sudut spesifik, samakan rentang_sudut_rotasi nya
# Sudut rotasi berlawanan jarum jam
rentang_sudut_rotasi = (-35, 35)
sudut_rotasi = random.randint(
  rentang_sudut_rotasi[0],
  rentang_sudut_rotasi[1]
)
skala = 0.7

# Matriks untuk menjalankan transformasi Affine (transformasi linear dan translasi)
# https://docs.opencv.org/3.4/d4/d61/tutorial_warp_affine.html
matriks_rotasi = cv2.getRotationMatrix2D(
    (panjang_image/2,tinggi_image/2),
    sudut_rotasi,
    skala
    )

image_dino_rotasi = cv2.warpAffine(image_dino_crop, matriks_rotasi,(panjang_image,tinggi_image))
cv2_imshow(image_dino_rotasi)
```

### Memutarbalikkan Image
```py
# flipCode 0 balik vertikal
image_dino_balik_vertikal = cv2.flip(image_dino_crop, flipCode=0)

# flipCode 1 balik horizontal
image_dino_balik_horizontal = cv2.flip(image_dino_crop, flipCode=1)

# flipCode -1 balik vertikal horizontal
image_dino_balik_vertikal_horizontal = cv2.flip(image_dino_crop, flipCode=-1)

cv2_imshow(image_dino_balik_vertikal)
cv2_imshow(image_dino_balik_horizontal)
```

## Manipulasi Pixel pada Image dengan Filter, Fungsi Kernel, dan Operator
> Fungsi Kernel merupakan fungsi yang mentransformasikan setiap elemen matriks menggunakan matriks transformasi. Pada kasus image processing / pemrosesan image, image merupakan bentuk matriks.

### Efek Blur
```py
tingkatan_blur = 20
image_dino_blur = cv2.blur(image_dino_crop, (tingkatan_blur, tingkatan_blur))
cv2_imshow(image_dino_blur)
```

### Efek Noise
```py
noise_gauss = np.random.normal(0,1,image_dino_crop.size)
noise_gauss = noise_gauss.reshape(image_dino_crop.shape[0],image_dino_crop.shape[1],image_dino_crop.shape[2]).astype('uint8')
image_dino_noise = image_dino_crop + image_dino_crop * noise_gauss

cv2_imshow(image_dino_noise)
```

### Efek Brightness dan Contrast
```py
brightness = 50
contrast = 130
image_dino_brightness_contrast = np.int16(image_dino_crop)
image_dino_brightness_contrast = image_dino_brightness_contrast * (contrast/127+1) - contrast + brightness
image_dino_brightness_contrast = np.clip(image_dino_brightness_contrast, 0, 255)
image_dino_brightness_contrast = np.uint8(image_dino_brightness_contrast)

cv2_imshow(image_dino_crop)
cv2_imshow(image_dino_brightness_contrast)
```

## Manipulasi Penambahan Elemen pada Image

## Menyimpan Image hasil pemrosesan

**8) Menyimpan image dengan nama dan ekstensi baru**
```py
alamat_file_baru = '/content/sample_data/dino_crop.png'

# Method imwrite digunakan untuk menyimpan file
cv2.imwrite(alamat_file_baru, image_dino_crop)
```

## Challenge
- Buat 60 image untuk dataset pengklasifikasi Domba vs Kambing
- Dari 3 image Domba dan 3 image Kambing
- Dengan cara mentransformasi image tersebut berdasarkan bentuk, posisi, filter, dan tambahan elemen

## Google Colab
- [Link](https://colab.research.google.com/drive/1k2HGSVxi9uWDL0_8o4jdr49aieWXBr6I?usp=sharing)

## Referensi
- [Geeksforgeeks - OpenCV Python](https://www.geeksforgeeks.org/opencv-python-tutorial/)
- [Geeksforgeeks - OpenCV Python - Overview](https://www.geeksforgeeks.org/opencv-overview/)
- [Geeksforgeeks - OpenCV Python - Introduction](https://www.geeksforgeeks.org/introduction-to-opencv/)
- [LearnOpenCV - Getting Started](https://learnopencv.com/getting-started-with-opencv/)
- [Kaggle - Image Augmentation with OpenCV](https://www.kaggle.com/code/hanzh0420/image-augmentation-with-opencv)
- [Keras - ImageDataGenerator](https://www.tensorflow.org/api_docs/python/tf/keras/preprocessing/image/ImageDataGenerator)
- [towardsdatascience - Complete Image Augmentation in OpenCV](https://towardsdatascience.com/complete-image-augmentation-in-opencv-31a6b02694f5)
- [Open CV Filtering](https://docs.opencv.org/4.x/d4/d13/tutorial_py_filtering.html)
